package types

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/cadyrov/goerr"
	"gitlab.com/teleport.media/validator.nodr/internal/domain"
	"time"
)

type Service struct {
	service Servicer
}

func (s *Service) mustEmbedUnimplementedValidateServer() {
	//TODO implement me
	panic("implement me")
}

func (s *Service) Report(ctx context.Context, in *ReportRequest) (*ReportResponse, error) {
	stats := make(domain.Stats, 0, len(in.Reports))

	for i := range in.Reports {
		st := domain.Stat{
			LicenseKey: in.Reports[i].SenderId,
			Account:    in.Reports[i].AccountId,
			ClientID:   in.Reports[i].ReceiverId,
			StreamID:   in.Reports[i].StreamId,
			SegmentID:  in.Reports[i].SegmentId,
			Size:       in.Reports[i].Size,
			CreatedAt:  time.Now(),
		}

		stats = append(stats, st)
	}

	s.service.RegisterStat(ctx, stats)

	return &ReportResponse{}, nil
}

var ErrCoreNotInitialized = errors.New("service not initialized")

func New(service Servicer) (*Service, error) {
	if service == nil {
		return nil, goerr.Internal{}.Wrap(ErrCoreNotInitialized, "new service")
	}

	return &Service{service: service}, nil
}

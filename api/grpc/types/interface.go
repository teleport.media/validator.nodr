package types

import (
	"context"
	"gitlab.com/teleport.media/validator.nodr/internal/domain"
)

type Servicer interface {
	RegisterStat(ctx context.Context, stat domain.Stats)
}

package handlers

import (
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"gitlab.com/teleport.media/validator.nodr/internal/domain"
	"net/http"
)

type AddStatRequest struct {
	SenderID   string `json:"sender_id"`   //ид нодра
	ReceiverID string `json:"receiver_id"` //ид браузера
	StreamID   string `json:"stream_id"`
	SegmentID  string `json:"segment_id"`
	Size       uint64 `json:"size"`
}

// AddStat
// @Title Add stat of clients
// @Description  Add stat of clients
// @Param  id  body  AddStatRequest  true  "JSON"
// @Success  200  object  MessageReply    "JSON"
// @Failure  400  object  Error  "ErrorResponse JSON"
// @Failure  401  object  Error  "ErrorResponse JSON"
// @Failure  404  object  Error  "ErrorResponse JSON"
// @Failure  409  object  Error  "ErrorResponse JSON"
// @Resource stat
// @Route /api/v1/stat [post].
func AddStat(srv StatsService, l *zerolog.Logger) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		adr := AddStatRequest{}

		if err := extractPayload(r, &adr); err != nil {
			WriteError(w, errors.Wrap(err, "handler"), l)

			return
		}

		dm := domain.Stat{
			LicenseKey: adr.SenderID,
			ClientID:   adr.ReceiverID,
			StreamID:   adr.StreamID,
			SegmentID:  adr.SegmentID,
			Size:       adr.Size,
		}

		if err := srv.AddStatClient(r.Context(), dm); err != nil {
			WriteError(w, errors.Wrap(err, "handler"), l)

			return
		}

		WriteOK(w, http.StatusOK, MessageReply{"success"}, l)
	}
}

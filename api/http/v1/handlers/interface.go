package handlers

import (
	"context"

	"gitlab.com/teleport.media/validator.nodr/internal/domain"
)

type StatsService interface {
	AddStatClient(ctx context.Context, stat domain.Stat) error
}

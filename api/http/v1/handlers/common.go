package handlers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"gitlab.com/cadyrov/goerr"
	"io/ioutil"
	"net/http"
	"strings"
)

var (
	errRequestBodyRead      = errors.New("request body read error")
	errRequestBodyMalformed = errors.New("request body malformed")
)

type Error struct {
	Message string            `json:"message"`
	Code    uint              `json:"code"`
	Details map[string]string `json:"details"`
}

type MessageReply struct {
	Message string `json:"message"`
}

// nolint: cyclop
func WriteError(w http.ResponseWriter, e error, log *zerolog.Logger) {
	err := Error{}

	if cause := errors.Cause(e); cause != nil {
		err.Message = cause.Error()
	} else {
		w.WriteHeader(http.StatusInternalServerError)

		log.Err(e).Msg("nil error response")

		return
	}

	bts, em := json.Marshal(err)
	if em != nil {
		w.WriteHeader(http.StatusInternalServerError)

		if _, err := w.Write([]byte(em.Error())); err != nil {
			log.Error().Err(err).Msg("write error response")
		}

		return
	}

	switch {
	case errors.As(e, &goerr.BadRequest{}):
		w.WriteHeader(http.StatusBadRequest)

		_, _ = w.Write(bts)
	case errors.As(e, &goerr.Forbidden{}):
		w.WriteHeader(http.StatusForbidden)

		_, _ = w.Write(bts)
	case errors.As(e, &goerr.NotAcceptable{}):
		w.WriteHeader(http.StatusNotAcceptable)

		_, _ = w.Write(bts)
	case errors.As(e, &goerr.Unauthorized{}):
		w.WriteHeader(http.StatusUnauthorized)
	case errors.As(e, &goerr.NotFound{}):
		w.WriteHeader(http.StatusNotFound)

		_, _ = w.Write(bts)
	case errors.As(e, &goerr.Conflict{}):
		w.WriteHeader(http.StatusConflict)

		_, _ = w.Write(bts)
	default:
		w.WriteHeader(http.StatusInternalServerError)
	}

	log.Error().Err(e).Msg("write error response")
}

func WriteOK(w http.ResponseWriter, status int, in interface{}, log *zerolog.Logger) {
	if in == nil {
		w.WriteHeader(status)

		return
	}

	bts, em := json.Marshal(in)
	if em != nil {
		w.WriteHeader(http.StatusInternalServerError)

		if _, err := w.Write([]byte(em.Error())); err != nil {
			log.Error().Err(err).Msg("write ok response")
		}

		return
	}

	if _, err := w.Write(bts); err != nil {
		w.WriteHeader(http.StatusInternalServerError)

		log.Error().Err(err).Msg("write ok response")
	}
}

func extractPayload(r *http.Request, in interface{}) error {
	bts, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return goerr.Internal{}.Wrap(errRequestBodyRead, "extract")
	}
	defer r.Body.Close()

	if err := json.Unmarshal(bts, in); err != nil {
		return goerr.Internal{}.Wrap(errRequestBodyMalformed, "extract")
	}

	return nil
}

func corsHeaders(responseWriter http.ResponseWriter, corsHeader string) {
	responseWriter.Header().Set("Access-Control-Allow-Origin", corsHeader)
	responseWriter.Header().Set("Access-Control-Allow-Credentials", "true")
	responseWriter.Header().Set("Access-Control-Allow-Methods", "GET,POST,PATCH,PUT,UPDATE,DELETE,OPTIONS")
	responseWriter.Header().Set("Access-Control-Allow-Headers", "Content-Type, X-CSRF-Key, Authorization, Cookie, Cache-Control, Pragma, Expires")
}

func CorsMiddleware(cors []string) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(responseWriter http.ResponseWriter, request *http.Request) {
			origin := checkCorsHeaders(cors, request)

			corsHeaders(responseWriter, origin)

			next.ServeHTTP(responseWriter, request)
		})
	}
}

func checkCorsHeaders(corses []string, request *http.Request) string {
	origin := request.Header.Get("Origin")

	for i := range corses {
		if strings.Contains(corses[i], origin) {
			return corses[i]
		}
	}

	return ""
}

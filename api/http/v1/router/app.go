package router

import (
	"gitlab.com/teleport.media/validator.nodr/api/http/v1/handlers"
	"gitlab.com/teleport.media/validator.nodr/cmd"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"gitlab.com/cadyrov/goerr"
)

type Service struct {
	statService handlers.StatsService
	logger      *zerolog.Logger
	config      cmd.Config
}

var (
	ErrServiceNil = goerr.Internal{}.Wrap(errors.New("service not initialized"), "route")
	ErrLoggerNil  = goerr.Internal{}.Wrap(errors.New("logger not initialized"), "route")
)

func New(config cmd.Config, logger *zerolog.Logger, srv handlers.StatsService) (*Service, error) {
	if logger == nil {
		return nil, ErrLoggerNil
	}

	if srv == nil {
		return nil, ErrServiceNil
	}

	if logger == nil {
		return nil, ErrLoggerNil
	}

	return &Service{
		statService: srv,
		logger:      logger,
		config:      config,
	}, nil
}

func (app *Service) Routes() *mux.Router {
	return app.initRoutes()
}

func (app *Service) initRoutes() *mux.Router {
	routes := mux.NewRouter()
	routes.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			app.logger.Trace().Interface("Headers", r.Header).Msgf("request %s", r.URL.Path)

			next.ServeHTTP(w, r)
		})
	})

	routes.Use(handlers.CorsMiddleware(app.config.Project.HTTP.Cors))

	routes.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		app.logger.Debug().Msgf("path %s not found", r.URL.Path)
		http.NotFound(w, r)
	})

	routes.MethodNotAllowedHandler = http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		if _, err := rw.Write([]byte("Method not allowed")); err != nil {
			app.logger.Err(err).Msg("error during write method not allowed response")
		}
	})

	sh := http.StripPrefix("/swagger-ui/", http.FileServer(http.Dir("/resources/swagger-ui")))
	routes.PathPrefix("/swagger-ui").Handler(sh)

	rootRouter := routes.PathPrefix("/api/v1").Subrouter()

	statRoute := rootRouter.PathPrefix("/stat").Subrouter()
	app.SetStatsRoutes(statRoute)

	return routes
}

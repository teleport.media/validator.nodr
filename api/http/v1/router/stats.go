package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/teleport.media/validator.nodr/api/http/v1/handlers"
)

func (app *Service) SetStatsRoutes(router *mux.Router) {
	router.HandleFunc("", handlers.AddStat(app.statService, app.logger)).Methods(http.MethodPost)
}

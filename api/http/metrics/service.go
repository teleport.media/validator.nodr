package metrics

import (
	"errors"

	"gitlab.com/cadyrov/goerr"
	"gitlab.com/teleport.media/validator.nodr/internal/service"
)

type Service struct {
	service *service.Service
}

var ErrCoreNotInitialized = errors.New("service not initialized")

func New(service *service.Service) (*Service, error) {
	if service == nil {
		return nil, goerr.Internal{}.Wrap(ErrCoreNotInitialized, "start service")
	}

	return &Service{service: service}, nil
}

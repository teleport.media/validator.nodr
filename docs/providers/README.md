[main](../../README.md)

# Providers which is needed to run app

- #Access provider  

    Access provider get list of active NODRes which can authorized
    
    Provider specification is [here](../../provider/access_list)
    
    Config is in [config.yml](../../config.yml) provider block

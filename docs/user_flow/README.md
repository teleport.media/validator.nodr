[main](../../README.md)

# Most important and often ways and processes by application

Application realize next methods by grpc:

Registry:

- application contains in-memory cache list of available NODRes from [access_list](../../provider/access_list)
provider and check every request by ip and license_key
- if data correct app get auth token jwt
- list refresh from provider time_by_time(internal/access_list/refresh duration in [config.yml](../../config.yml))

Check:

- check jwt token


Application realize next methods by http:

- metrics by prometheus by route "/metrics"
[main](../../README.md)

# Structure of application

[./api](../../api) contains api specification for grpc and http server

[./bin](../../bin) contains built binary for run app

[./cmd](../../cmd) contains endpoints to run app

[./docs](../../docs) contains documentations 

[./internal](../../internal) contains logic of project 

[./provider](../../provider) contains needed integrations like: foreign services, databases etc

[./resources](../../resources) contains resources like: images, configs, static files, multulanguage etc

[./tests](../../tests) contains tests of app

[Dockerfile](../../Dockerfile) Dockerfile

[config.yml](../../config.yml) working config file

[main.go](../../main.go) main endpoint

[Makefile](../../Makefile) basic Makefile to most support operations





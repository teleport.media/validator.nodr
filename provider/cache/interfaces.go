package cache

import (
	"context"
	"time"
)

type Cache interface {
	Set(ctx context.Context, key string, in interface{}, ttl time.Duration) error
	Get(ctx context.Context, key string) (string, bool)
	Keys(ctx context.Context, key string) ([]string, error)
	Del(ctx context.Context, keys ...string) error
}

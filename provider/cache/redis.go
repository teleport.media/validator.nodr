package cache

import (
	"context"
	"encoding/json"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"gitlab.com/cadyrov/goerr"
)

type Redis struct {
	client *redis.Client
	lg     *zerolog.Logger
}

const (
	readTimeoutRedisCache  = time.Second
	writeTimeoutRedisCache = time.Second * 6
)

func New(endpoint, password string, db int, lg *zerolog.Logger) (*Redis, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr:         endpoint,
		Password:     password,
		DB:           db,
		ReadTimeout:  readTimeoutRedisCache,
		WriteTimeout: writeTimeoutRedisCache,
	})

	if err := rdb.Ping(context.Background()).Err(); err != nil {
		return nil, goerr.Internal{}.Wrap(err, "ping to redis")
	}

	return &Redis{client: rdb, lg: lg}, nil
}

func (r *Redis) Del(ctx context.Context, keys ...string) error {
	_, err := r.client.Del(ctx, keys...).Result()

	return errors.Wrap(err, "del redis keys")
}

func (r *Redis) Set(ctx context.Context, key string, in interface{}, ttl time.Duration) error {
	b, err := json.Marshal(in)
	if err != nil {
		return errors.Wrap(err, "redis set")
	}

	r.client.Set(ctx, key, b, ttl)

	return nil
}

func (r *Redis) Get(ctx context.Context, key string) (string, bool) {
	val, err := r.client.Get(ctx, key).Result()
	if errors.Is(err, redis.Nil) {
		return "", false
	} else if err != nil {
		r.lg.Err(err).Msg("cache error")

		return "", false
	}

	return val, true
}

func (r *Redis) Keys(ctx context.Context, key string) ([]string, error) {
	val, err := r.client.Keys(ctx, key).Result()

	return val, errors.Wrap(err, "redis getKeys")
}

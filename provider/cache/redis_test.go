package cache

import (
	"context"
	"testing"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRedis(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	t.Parallel()

	rdb := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
		DB:   4,
	})

	ctx := context.Background()

	err := rdb.Set(ctx, "key", "value", time.Second).Err()
	require.Nil(t, err)

	val, err := rdb.Get(ctx, "key").Result()
	require.Nil(t, err)
	assert.Equal(t, val, "value")

	_, err = rdb.Get(ctx, "key2").Result()
	require.Equal(t, err, redis.Nil)

	err = rdb.Set(ctx, "key3:key:key4", "value", time.Second).Err()
	require.Nil(t, err)

	_, err = rdb.Get(ctx, "key3:key:key4").Result()
	require.Nil(t, err)

	scl := rdb.Keys(ctx, "key3:*")
	require.Nil(t, scl.Err())
	require.Equal(t, len(scl.Val()), 1)

	err = rdb.Del(ctx, "key3.*").Err()
	require.Nil(t, err)

	scl = rdb.Keys(ctx, "key3")
	require.Nil(t, scl.Err())
	require.Equal(t, len(scl.Val()), 0)
}

package hedera

import (
	sdk "github.com/hashgraph/hedera-sdk-go/v2"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"gitlab.com/cadyrov/goerr"
	"time"

	"github.com/holiman/uint256"
)

type Service struct {
	accountID       string
	privateKey      string
	topic           string
	contractID      string
	treasuryP       string
	gas             uint64
	maxQueryPayment float64
	log             *zerolog.Logger
	client          *sdk.Client
}

func New(lg *zerolog.Logger, accountID, privateKey, topic, contractID, treasuryP string, gas uint64,
	maxQueryPayment float64) *Service {
	myAccountId, err := sdk.AccountIDFromString(accountID)
	if err != nil {
		panic(err)
	}

	myPrivateKey, err := sdk.PrivateKeyFromString(privateKey)
	if err != nil {
		panic(err)
	}

	client := sdk.ClientForTestnet()
	client.SetOperator(myAccountId, myPrivateKey)

	return &Service{
		accountID:       accountID,
		privateKey:      privateKey,
		topic:           topic,
		gas:             gas,
		maxQueryPayment: maxQueryPayment,
		log:             lg,
		client:          client,
		contractID:      contractID,
		treasuryP:       treasuryP,
	}
}

func (s *Service) SendMessageToTopic(msg []byte) error {
	topicID, err := sdk.TopicIDFromString(s.topic)
	if err != nil {
		return goerr.Internal{}.Wrap(err, "send message to topic, find topic")
	}

	_, err = sdk.NewTopicMessageSubmitTransaction().
		SetMessage(msg).
		// To which topic ID
		SetTopicID(topicID).
		Execute(s.client)

	if err != nil {
		return goerr.Internal{}.Wrap(err, "send message to topic")
	}

	return nil
}

func (s *Service) Subscribe(from time.Time, f func(content []byte)) error {
	topicID, err := sdk.TopicIDFromString(s.topic)
	if err != nil {
		return goerr.Internal{}.Wrap(err, "send message to topic, find topic")
	}

	if _, err := sdk.NewTopicMessageQuery().
		// For which topic ID
		SetTopicID(topicID).
		// When to start
		SetStartTime(from).
		Subscribe(s.client, func(message sdk.TopicMessage) {
			f(message.Contents)
		}); err != nil {
		return goerr.Internal{}.Wrap(err, "start to subscribe")
	}

	return nil
}

const contractFunc = "trafficCommit"

func (s *Service) ContractSize(accountID string, size uint64) error {
	ctrID, err := sdk.ContractIDFromString(s.contractID)
	if err != nil {
		return errors.Wrap(err, "contract size")
	}

	accID, err := sdk.AccountIDFromString(accountID)
	if err != nil {
		panic(err)
	}

	solAddress := accID.ToSolidityAddress()

	contractFunctionParams, err := sdk.NewContractFunctionParameters().AddAddress(solAddress)
	if err != nil {
		return errors.Wrap(err, "contract size")
	}

	uib := uint256.NewInt(size)

	bts := uib.Bytes32()

	contractFunctionParams = contractFunctionParams.AddUint256(bts[:])

	tx, err := sdk.NewContractExecuteTransaction().
		SetContractID(ctrID).
		SetGas(s.gas).
		SetFunction(contractFunc, contractFunctionParams).
		SetMaxTransactionFee(sdk.NewHbar(s.maxQueryPayment)).
		FreezeWith(s.client)
	if err != nil {
		s.log.Debug().Err(err).Msg("ContractSize")

		return goerr.Internal{}.Wrap(err, "ContractSize")
	}

	pkey, err := sdk.PrivateKeyFromString(s.treasuryP)
	if err != nil {
		s.log.Debug().Err(err).Msg("ContractSize")

		return goerr.Internal{}.Wrap(err, "ContractSize")
	}

	tx = tx.Sign(pkey)

	res, err := tx.Execute(s.client)
	if err != nil {
		s.log.Debug().Err(err).Msg("ContractSize")

		return goerr.Internal{}.Wrap(err, "ContractSize")
	}

	s.log.Debug().Interface("result", res).Msg("ContractSize")

	return nil
}

package hedera

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"
	"time"

	sdk "github.com/hashgraph/hedera-sdk-go/v2"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/suite"
	"gitlab.com/teleport.media/validator.nodr/internal/domain"
)

type HederaSuite struct {
	suite.Suite
	accountID, privateKey, topic, contractID string
	gas                                      uint64
	maxQueryPayment                          float64
	service                                  *Service
	log                                      *zerolog.Logger
	aimAccount                               string
	treasuryP                                string
}

func (suite *HederaSuite) SetupTest() {
	lg := zerolog.New(os.Stderr).With().Timestamp().Logger().Level(zerolog.DebugLevel)

	suite.accountID = "0.0.34189949"
	suite.privateKey = "302e020100300506032b6570042204206bfe352361e93dff03e665b3a4d085da54bd337cb4a82b54667909da356d17ef"
	suite.topic = "0.0.34753975"
	suite.contractID = "0.0.34398679"
	suite.gas = 1000000
	suite.maxQueryPayment = 0.75
	suite.log = &lg
	suite.treasuryP = "302e020100300506032b657004220420b9c29d502de05a77016bc4680768ef73bf357dbb2731bf05467b112d61648874"
	suite.aimAccount = "0.0.34227422"

	suite.service = New(suite.log, suite.accountID, suite.privateKey, suite.topic,
		suite.contractID, suite.treasuryP, suite.gas, suite.maxQueryPayment)
}

func TestHedera(t *testing.T) {
	suite.Run(t, new(HederaSuite))
}

func (suite *HederaSuite) TestService_SendMessageToTopic() {
	stat := domain.Stat{
		LicenseKey: "testkey",
		StreamID:   "test stream",
		SegmentID:  "test segment",
		Size:       1,
		CreatedAt:  time.Now(),
	}

	bt, err := json.Marshal(stat)
	suite.Require().Nil(err)

	err = suite.service.SendMessageToTopic(bt)

	fmt.Println(err)

	suite.Require().Nil(err)

	f := func(content []byte) {
		st := domain.Stat{}
		err := json.Unmarshal(content, &st)

		fmt.Println(st, err)

		suite.Require().Nil(err)
	}

	err = suite.service.Subscribe(time.Time{}, f)

	fmt.Println(err)

	suite.Require().Nil(err)

	time.Sleep(time.Second * 5)
}

func (suite *HederaSuite) TestService_SendContract() {
	if testing.Short() {
		suite.T().Skip()
	}

	err := suite.service.ContractSize(suite.aimAccount, 1000)

	suite.Nil(err)
}

func (suite *HederaSuite) TestCreateTopic() {
	if testing.Short() {
		suite.T().Skip()
	}

	myAccountId, err := sdk.AccountIDFromString(suite.accountID)
	if err != nil {
		panic(err)
	}

	myPrivateKey, err := sdk.PrivateKeyFromString(suite.privateKey)
	if err != nil {
		panic(err)
	}

	fmt.Printf("The account ID is = %v\n", myAccountId)
	fmt.Printf("The private key is = %v\n", myPrivateKey)

	client := sdk.ClientForTestnet()
	client.SetOperator(myAccountId, myPrivateKey)

	transactionResponse, err := sdk.NewTopicCreateTransaction().
		SetTransactionMemo("TLPRT topic").
		SetAdminKey(client.GetOperatorPublicKey()).
		Execute(client)

	if err != nil {
		println(err.Error(), ": error creating topic")
		return
	}

	// Get the receipt
	transactionReceipt, err := transactionResponse.GetReceipt(client)

	if err != nil {
		println(err.Error(), ": error getting topic create receipt")
		return
	}

	topicID := *transactionReceipt.TopicID

	fmt.Printf("topicID: %v\n", topicID)
}

func (suite *HederaSuite) TestReadContract() {
	if testing.Short() {
		suite.T().Skip()
	}

	ctrID, err := sdk.ContractIDFromString(suite.contractID)
	suite.Require().Nil(err)

	myAccountId, err := sdk.AccountIDFromString(suite.accountID)
	suite.Require().Nil(err)

	myPrivateKey, err := sdk.PrivateKeyFromString(suite.privateKey)
	suite.Require().Nil(err)

	client := sdk.ClientForTestnet()
	client.SetOperator(myAccountId, myPrivateKey)

	_, err = sdk.NewContractExecuteTransaction().
		SetContractID(ctrID).
		SetGas(1000000).
		SetFunction("getCurrentStat", nil).
		SetMaxTransactionFee(sdk.NewHbar(0.75)).
		Execute(client)

	suite.Nil(err)
}

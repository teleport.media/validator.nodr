package cmd

import "time"

type Config struct {
	Project Project `mapstructure:"project"`
}

type Project struct {
	Name     string   `mapstructure:"name"`
	Provider Provider `mapstructure:"provider"`
	Log      Log      `mapstructure:"log"`
	GRPC     GRPC     `mapstructure:"grpc"`
	Metrics  Metrics  `mapstructure:"metrics"`
	HTTP     HTTP     `mapstructure:"http"`
}

type Provider struct {
	Hedera Hedera `mapstructure:"hedera"`
	Cache  Cache  `mapstructure:"cache"`
}

type Cache struct {
	Endpoint string `mapstructure:"endpoint"`
	Password string `mapstructure:"password"`
	Bucket   int    `mapstructure:"bucket"`
}

type Hedera struct {
	AccountID       string  `mapstructure:"account_id"`
	PrivateKey      string  `mapstructure:"private_key"`
	Topic           string  `mapstructure:"topic"`
	ContractID      string  `mapstructure:"contract_id"`
	TreasuryP       string  `mapstructure:"treasury_p"`
	Gas             uint64  `mapstructure:"gas"`
	MaxQueryPayment float64 `mapstructure:"max_query_payment"`
}

type HTTP struct {
	Host         string        `mapstructure:"host"`
	Port         int           `mapstructure:"port"`
	WriteTimeout time.Duration `mapstructure:"write_timeout"`
	ReadTimeout  time.Duration `mapstructure:"read_timeout"`
	IdleTimeout  time.Duration `mapstructure:"idle_timeout"`
	SSL          bool          `mapstructure:"ssl"`
	SSLCertPath  string        `mapstructure:"ssl_cert_path"`
	SSLKeyPath   string        `mapstructure:"ssl_key_path"`
	Cors         []string      `mapstructure:"cors"`
}

type GRPC struct {
	Host string `mapstructure:"host"`
	Port int    `mapstructure:"port"`
}

type Log struct {
	Level string `mapstructure:"level"`
}

type Metrics struct {
	Path string `mapstructure:"path"`
	Host string `mapstructure:"host"`
	Port int    `mapstructure:"port"`
}

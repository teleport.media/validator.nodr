package grpc

import (
	"context"
	"fmt"
	"gitlab.com/teleport.media/validator.nodr/provider/cache"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"

	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
	"gitlab.com/teleport.media/validator.nodr/api/grpc/types"
	"gitlab.com/teleport.media/validator.nodr/api/http/v1/handlers"
	"gitlab.com/teleport.media/validator.nodr/api/http/v1/router"
	"gitlab.com/teleport.media/validator.nodr/cmd"
	"gitlab.com/teleport.media/validator.nodr/internal/service"
	"gitlab.com/teleport.media/validator.nodr/provider/hedera"
	"google.golang.org/grpc"
)

// nolint: gochecknoglobals
var (
	nodrCmd = &cobra.Command{
		Use:   "validator",
		Short: "Grpc auth implements",
		Long:  `Grpc auth implements validator`,
		Run: func(cmd *cobra.Command, args []string) {
			run()
		},
	}
)

// nolint:gochecknoinits
func init() {
	nodrCmd.PersistentFlags().StringVar(&cfgFileName, "config-file-name",
		"config", "set alternative config file name without extensions")
	nodrCmd.PersistentFlags().StringVar(&cfgFilePath, "config-file-path",
		".", "set alternative relative config file path")
	nodrCmd.PersistentFlags().StringVar(&envPrefix, "env-prefix",
		"VALIDATOR", "set alternative env prefix")

	cobra.OnInitialize(initConfig)
}

func run() {
	lg := initLogger()

	lg.Debug().Interface("full", config).Msg("config")

	hdr := config.Project.Provider.Hedera
	provider := hedera.New(&lg, hdr.AccountID, hdr.PrivateKey, hdr.Topic, hdr.ContractID, hdr.TreasuryP,
		hdr.Gas, hdr.MaxQueryPayment)

	chc := config.Project.Provider.Cache

	cacheService, e := cache.New(chc.Endpoint, chc.Password, chc.Bucket, &lg)
	if e != nil {
		lg.Fatal().Err(e).Msg("service")
	}

	serv, e := service.New(
		&lg,
		provider,
		cacheService,
	)
	if e != nil {
		lg.Fatal().Err(e).Msg("service")
	}

	stopWaitGroup := &sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())

	// serve prometheus
	stopWaitGroup.Add(1)
	http.Handle(config.Project.Metrics.Path, promhttp.Handler())

	metricsHTTPServer := http.Server{Addr: fmt.Sprintf("%s:%d",
		config.Project.Metrics.Host, config.Project.Metrics.Port)}

	go func() {
		defer stopWaitGroup.Done()
		lg.Debug().Msgf("serving metrics at: %s:%d",
			config.Project.Metrics.Host, config.Project.Metrics.Port)

		if err := metricsHTTPServer.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			lg.Error().Err(err).Msg("metrics serving error")
		}
	}()

	// serve grpc
	serverGRPC := grpc.NewServer()

	srv, err := types.New(serv)
	if err != nil {
		lg.Err(err)
	}

	types.RegisterValidateServer(serverGRPC, srv)

	l, err := net.Listen("tcp", fmt.Sprintf("%s:%d", config.Project.GRPC.Host, config.Project.GRPC.Port))
	if err != nil {
		lg.Err(err).Msg("cmd grpc")
	}

	go func() {
		defer stopWaitGroup.Done()
		lg.Debug().Msgf("serving grpc at: %s:%d", config.Project.GRPC.Host, config.Project.GRPC.Port)

		if err := serverGRPC.Serve(l); err != nil {
			lg.Err(err).Msg("cmd grpc")
		}
	}()

	httpServ, err := initHTTPServer(&lg, config, serv)
	if err != nil {
		lg.Fatal().Err(e).Msg("http service")
	}

	stopWaitGroup.Add(1)

	go func() {
		defer stopWaitGroup.Done()
		lg.Debug().Err(err).Msgf("serving http at: 0.0.0.0:%d", config.Project.HTTP.Port)

		if config.Project.HTTP.SSL {
			if err := httpServ.ListenAndServeTLS(config.Project.HTTP.SSLCertPath, config.Project.HTTP.SSLKeyPath); err != nil {
				lg.Error().Err(err).Msg("http serving error")
			}
		}

		if err := httpServ.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			lg.Error().Err(err).Msg("http serving error")
		}
	}()

	stopWaitGroup.Add(1)

	SigTermHandler(func() {
		defer stopWaitGroup.Done()
		if err := metricsHTTPServer.Shutdown(ctx); err != nil {
			lg.Error().Err(err).Msg("metrics server shutdown")
		}
		// grpc server stop
		serverGRPC.GracefulStop()
		// cancelling root contest

		if err := httpServ.Shutdown(ctx); err != nil {
			lg.Error().Err(err).Msg("metrics server shutdown")
		}
		// cancelling root contest
		cancel()
	}, lg)

	stopWaitGroup.Wait()
}

func SigTermHandler(stopFunc func(), log zerolog.Logger) {
	maxTermChanLen := 10

	termCh := make(chan os.Signal, maxTermChanLen)
	signal.Notify(termCh, os.Interrupt, syscall.SIGTERM|syscall.SIGKILL)

	go func() {
		sig := <-termCh
		log.Debug().Msgf("signal %s received, terminating...", sig)
		stopFunc()
	}()
}

func initHTTPServer(lg *zerolog.Logger, config cmd.Config, statsService handlers.StatsService) (*http.Server, error) {
	rout, err := router.New(config, lg, statsService)
	if err != nil {
		return nil, errors.Wrap(err, "init")
	}

	routes := rout.Routes()

	_ = lg

	return &http.Server{
		Handler:      routes,
		Addr:         ":" + strconv.Itoa(config.Project.HTTP.Port),
		ReadTimeout:  config.Project.HTTP.ReadTimeout,
		WriteTimeout: config.Project.HTTP.WriteTimeout,
		IdleTimeout:  config.Project.HTTP.IdleTimeout,
	}, nil
}

package grpc

import (
	"fmt"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/teleport.media/validator.nodr/cmd"
	"os"
	"strings"

	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
)

// nolint: gochecknoglobals
var (
	cfgFileName = ""
	cfgFilePath = ""
	envPrefix   = "VALIDATOR"
	config      = cmd.Config{}

	rootCmd = &cobra.Command{
		Use:   "root",
		Short: "root",
		Long:  `root has no implements`,
		Run:   func(cmd *cobra.Command, args []string) {},
	}
)

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

// nolint:gochecknoinits
func init() {
	rootCmd.AddCommand(nodrCmd)
}

func initLogger() zerolog.Logger {
	var level zerolog.Level

	switch config.Project.Log.Level {
	case "debug":
		level = zerolog.DebugLevel
	case "warning":
		level = zerolog.WarnLevel
	case "error":
		level = zerolog.ErrorLevel
	default:
		level = zerolog.InfoLevel
	}

	zerolog.SetGlobalLevel(level)

	return zerolog.New(os.Stdout).With().Timestamp().Logger().Level(level)
}

func initConfig() {
	v := viper.New()

	v.SetConfigName(cfgFileName)

	v.AddConfigPath(cfgFilePath)

	var errViperCNF *viper.ConfigFileNotFoundError
	if err := v.ReadInConfig(); err != nil {
		// It's okay if there isn't a config file
		if errors.As(err, &errViperCNF) {
			panic(err)
		}
	}

	v.SetEnvPrefix(envPrefix)

	v.AutomaticEnv()

	r := strings.NewReplacer(".", "_")

	v.SetEnvKeyReplacer(r)

	if err := v.Unmarshal(&config); err != nil {
		panic(err)
	}
}

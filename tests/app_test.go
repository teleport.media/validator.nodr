package tests

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/require"
	"gitlab.com/teleport.media/validator.nodr/api/grpc/types"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
)

func TestApp(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	ctx := context.Background()

	r := strings.NewReader(`{
		"sender_id":"e75655ea52428dac",
		"receiver_id":"f2ef88e5fefc3a3d",
		"stream_id":"66e8b3ab",
		"segment_id":"6f2484e9",
		"size":631868
	}`)

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, "https://validator1.nodr.app/api/v1/stat", r)
	//req, err := http.NewRequestWithContext(ctx, http.MethodPost, "http://0.0.0.0:8080/api/v1/stat", r)
	require.Nil(t, err)

	client := http.DefaultClient
	res, err := client.Do(req)
	require.Nil(t, err)

	defer res.Body.Close()

	resp, err := ioutil.ReadAll(res.Body)
	require.Nil(t, err)
	require.Equal(t, res.StatusCode, http.StatusOK)

	fmt.Println(resp)

	conn, err := grpc.Dial("grpc.validator1.nodr.app:8090",
		//conn, err := grpc.Dial("0.0.0.0:8090",
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	require.Nil(t, err)

	defer conn.Close()

	clnt := types.NewValidateClient(conn)

	rps := []*types.Report{
		{
			SenderId:   "e75655ea52428dac",
			AccountId:  "0.0.34227422",
			ReceiverId: "f2ef88e5fefc3a3d",
			StreamId:   "66e8b3ab",
			SegmentId:  "6f2484e9",
			Size:       631868,
		},
	}

	rsp, err := clnt.Report(ctx, &types.ReportRequest{Reports: rps})
	require.Nil(t, err)

	fmt.Println(rsp)
}

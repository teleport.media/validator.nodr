swagger:
	rm -f ./resources/oas.json && \
	goas --module-path . --main-file-path ./api/http/v1/apidoc.go -output ./resources/oas.json
lint:
	gofmt -s -w ./ && golangci-lint run
gen-proto:
	protoc  --go-grpc_out=. --go_out=. ./api/grpc/proto/*.proto
build:
	go build -o ./bin/validator ./main.go
run:
	./bin/validator validator
run-ssl:
	export VALIDATOR_PROJECT_HTTP_SSL=true && export VALIDATOR_PROJECT_HTTP_PORT=443 && export VALIDATOR_PROJECT_HTTP_SSL_KEY_PATH=/usr/local/validator.nodr/resources/config/ssl/privkey1.pem && export VALIDATOR_PROJECT_HTTP_SSL_CERT_PATH=/usr/local/validator.nodr/resources/config/ssl/fullchain1.pem && ./bin/validator validator
test:
	go test -v --short ./...
package domain

import (
	"errors"
	"fmt"
	"time"
)

type Stat struct {
	LicenseKey string
	Account    string
	ClientID   string
	StreamID   string
	SegmentID  string
	Size       uint64
	CreatedAt  time.Time
}

var ErrNotValid = errors.New("stat not valid")

func (s Stat) Validate() error {
	if s.LicenseKey == "" {
		return ErrNotValid
	}

	if s.ClientID == "" {
		return ErrNotValid
	}

	if s.StreamID == "" {
		return ErrNotValid
	}

	if s.SegmentID == "" {
		return ErrNotValid
	}

	if s.Size == 0 {
		return ErrNotValid
	}

	if s.CreatedAt.IsZero() {
		return ErrNotValid
	}

	return nil
}

type Stats []Stat

func (s Stat) Key(prefix string) string {
	return fmt.Sprintf("%s:%s:%s:%s:%s", prefix, s.LicenseKey, s.ClientID, s.StreamID, s.SegmentID)
}

package service

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"gitlab.com/cadyrov/goerr"
)

type JwtOption struct {
	Issuer    string
	SecretKey string
	Expired   time.Duration
}

type Service struct {
	logger   *zerolog.Logger
	cache    Cache
	provider CryptoProvider
}

var (
	ErrLoggerNotInitialized    = errors.New("logger not initialized")
	ErrProviderNotInitialized  = errors.New("crypto provider not initialized")
	ErrCacheNotInitialized     = errors.New("cache provider not initialized")
	ErrCannotSubscribeForTopic = errors.New("cannot subscribe for topic")
)

func New(log *zerolog.Logger, provider CryptoProvider, cache Cache) (*Service, error) {
	if log == nil {
		return nil, goerr.Internal{}.Wrap(ErrLoggerNotInitialized, "service new")
	}

	if provider == nil {
		return nil, goerr.Internal{}.Wrap(ErrProviderNotInitialized, "service new")
	}

	if cache == nil {
		return nil, goerr.Internal{}.Wrap(ErrCacheNotInitialized, "service new")
	}

	s := &Service{
		logger:   log,
		provider: provider,
		cache:    cache,
	}

	if err := provider.Subscribe(time.Now(), s.StatContentFromQuery); err != nil {
		return nil, goerr.Internal{}.Wrap(ErrCannotSubscribeForTopic, "service new")
	}

	go func() {
		ctx := context.Background()

		t := time.NewTicker(time.Second * 5)

		for range t.C {
			s.validate(ctx)
		}
	}()

	return s, nil
}

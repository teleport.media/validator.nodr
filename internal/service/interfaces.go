package service

import (
	"context"
	"time"
)

type CryptoProvider interface {
	SendMessageToTopic(msg []byte) error
	Subscribe(from time.Time, f func(content []byte)) error
	ContractSize(accountID string, size uint64) error
}

type Cache interface {
	Set(ctx context.Context, key string, in interface{}, ttl time.Duration) error
	Get(ctx context.Context, key string) (string, bool)
	Keys(ctx context.Context, key string) ([]string, error)
	Del(ctx context.Context, keys ...string) error
}

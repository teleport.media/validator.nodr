package service

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/cadyrov/goerr"
	"gitlab.com/teleport.media/validator.nodr/internal/domain"
	"strings"
	"time"
)

const (
	NODRCachePrefix   = "nodr"
	ClientCachePrefix = "client"
)

// RegisterStat
// get request from NODR an compare with oracle
func (s *Service) RegisterStat(ctx context.Context, stat domain.Stats) {
	for i := range stat {
		err := s.cache.Set(ctx, stat[i].Key(NODRCachePrefix), stat[i], time.Minute)
		if err != nil {
			s.logger.Err(err).Msg("Register stat")

			continue
		}
	}

	s.logger.Debug().Interface("stats", stat).Msg("add stat")
}

// AddStatClient
// get request from client an send to HEDERA CONSENSUS SERVICE
func (s *Service) AddStatClient(ctx context.Context, stat domain.Stat) error {
	stat.CreatedAt = time.Now()

	if err := stat.Validate(); err != nil {
		return goerr.BadRequest{}.Wrap(err, "add stat to client")
	}

	/**
	bt, err := json.Marshal(stat)

	if err != nil {
		return goerr.Internal{}.Wrap(err, "add stat to client")
	}

	if err = s.provider.SendMessageToTopic(bt); err != nil {
		return goerr.Internal{}.Wrap(err, "add stat to client")
	}
	*/

	err := s.cache.Set(context.Background(), stat.Key(ClientCachePrefix), stat, time.Minute)
	if err != nil {
		s.logger.Err(err).Msg("Register stat")
	}

	return nil
}

func (s *Service) StatContentFromQuery(content []byte) {
	st := domain.Stat{}

	err := json.Unmarshal(content, &st)
	if err != nil {
		s.logger.Err(err).Msg("error while parsing msg")

		return
	}

	err = s.cache.Set(context.Background(), st.Key(ClientCachePrefix), st, time.Minute)
	if err != nil {
		s.logger.Err(err).Msg("Register stat")
	}

	s.logger.Debug().Interface("msg stat", st).Msg("content")
}

func (s *Service) validate(ctx context.Context) {
	toAccount := make(map[string]uint64)
	keysClient, err := s.cache.Keys(ctx, fmt.Sprintf("%s:*", ClientCachePrefix))
	if err != nil {
		s.logger.Err(err).Msg("validate")

		return
	}

	keysNodr, err := s.cache.Keys(ctx, fmt.Sprintf("%s:*", NODRCachePrefix))
	if err != nil {
		s.logger.Err(err).Msg("validate")

		return
	}

	s.logger.Debug().Strs("nodr", keysNodr).Strs("client", keysClient).Msg("validate")

	for i := range keysClient {
		s.logger.Debug().Str("keysClient", keysClient[i]).Msg("validate")

		nodrKey := fmt.Sprintf("%s:%s", NODRCachePrefix,
			strings.ReplaceAll(keysClient[i], "client:", ""))

		s.logger.Debug().Str("nodrKey", nodrKey).Msg("validate")

		nodrStat, ok := s.cache.Get(ctx, nodrKey)
		if !ok {
			continue
		}

		keyStat, ok := s.cache.Get(ctx, keysClient[i])
		if !ok {
			continue
		}

		err := s.cache.Del(ctx, keysClient[i])
		if err != nil {
			s.logger.Err(err).Msg("validate")

			continue
		}

		err = s.cache.Del(ctx, nodrKey)
		if err != nil {
			s.logger.Err(err).Msg("validate")

			continue
		}

		s.logger.Debug().Str("ndr", nodrKey).Str("cl", keysClient[i]).Msg("deleted keys")

		accCl := domain.Stat{}

		if err = json.Unmarshal([]byte(keyStat), &accCl); err != nil {
			s.logger.Err(err).Str("val", keyStat).Msg("validate client")

			continue
		}

		accNd := domain.Stat{}

		if err = json.Unmarshal([]byte(nodrStat), &accNd); err != nil {
			s.logger.Err(err).Str("val", nodrStat).Msg("validate nodr")

			continue
		}

		toAccount[accNd.Account] = toAccount[accNd.Account] + accCl.Size
	}

	s.logger.Debug().Interface("to account", toAccount).Msg("accounting")

	s.account(toAccount)
}

func (s *Service) account(toAccount map[string]uint64) {
	for k, v := range toAccount {
		if err := s.provider.ContractSize(k, v); err != nil {
			s.logger.Err(err).Msg("account")
		} else {
			s.logger.Debug().Str("account", k).Uint64("size", v).Msg("contractes")
		}
	}
}

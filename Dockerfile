FROM golang:1.17.3-alpine3.15 AS build

WORKDIR /go/src/nodr/
COPY . /go/src/nodr/
RUN go mod vendor
RUN apk add --no-cache make
RUN apk add --no-cache build-base
RUN make build

EXPOSE 8090
EXPOSE 8080

ENTRYPOINT ["make", "run"]
